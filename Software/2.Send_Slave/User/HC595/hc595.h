/*
 * @Author       : error: git config user.name & please set dead value or install git
 * @LastEditors  : error: git config user.name & please set dead value or install git
 * @LastEditTime : 2023-10-05 14:27:49
 * @FilePath     : \multiplex_controller\User\HC595\hc595.h
 * @Description  : 
 */
#ifndef __HC595_H
#define __HC595_H

#include "spi.h"

#define HC595_SPI &hspi1

void HC595_WriteByte(uint8_t data);
void HC595_Write(uint8_t channel, uint8_t state);
void HC595_Toggle(uint8_t channel);

#endif

