/*
 * @Author       : Atonidas
 * @LastEditors  : error: git config user.name & please set dead value or install git
 * @LastEditTime : 2023-10-05 15:19:51
 * @FilePath     : \multiplex_controller\User\Encoder\EncoderApp.c
 * @Description  : 编码器用户函数，请在此处具体完成编码器子项的注册函数及回调函数
 */

#include "Encoder\Encoder.h"
#include "Task\task.h"
#include "User\user.h"
#include "OLED\oled.h"

void encoder_Menu(void);
void encoder_MenuSub(void); // 显示参数demo

// 以下用于具体功能

void setChannelNums(void);
void setIntervalTime(void);
void setGroupInterval(void);
void setRuntime(void);
void clearData(void);
void setMode(void);

void Encoder_Init(void)
{
    registerEncoder(1, 6, 1, encoder_Menu, "Main");                  // Menu: 1~6
    registerEncoder(0, 8, 1, setChannelNums, "Channel Nums");        // Nums: 1~6
    registerEncoder(30, 600, 1, setIntervalTime, "Interval Time");   // Interval: Init:3.0s Max:60.0s
    registerEncoder(10, 100, 1, setGroupInterval, "Group Interval"); // Group: Init:1.0s Max:10.0s
    registerEncoder(20, 50, 1, setRuntime, "Run Time");              // Run: Init:2.0s Max:5.0s
    registerEncoder(0, 1, 1, clearData, "Clear Data");               // 0 or 1
    registerEncoder(0, 2, 1, setMode, "Sys Mode");                   // 0: Send_main 1:Send 2:receive
}

void encoder_thread_entry(void *parameter)
{
    Encoder_Init();
    rt_kprintf("encoder Init is ok\r\n");
    for (;;) {
        encoderServiceTask();
    }
}

void encoder_Menu(void)
{
    Encoder_MainMenu();
}

void encoder_MenuSub(void)
{
    uint8_t MenuSubNum = getEncoderValue(0);
    OLED_ShowNumber(0, 32, (int)getEncoderValue(MenuSubNum), 3, 15, 0);
}

void setChannelNums(void)
{
    OLED_ShowNumber(0, 32, (int)getEncoderValue(1), 3, 15, 0);
}
void setIntervalTime(void)
{
    OLED_ShowNumber(0, 32, (int)getEncoderValue(2), 3, 15, 0);
}
void setGroupInterval(void)
{
    OLED_ShowNumber(0, 32, getEncoderValue(3), 3, 15, 0);
}
void setRuntime(void)
{
    OLED_ShowNumber(0, 32, (int)getEncoderValue(4), 3, 15, 0);
}
void clearData(void)
{
    OLED_ShowNumber(0, 32, (int)getEncoderValue(5), 3, 15, 0);
}
void setMode(void)
{
    OLED_FillArea(0, 32, 160, 16, 0);
    switch (getEncoderValue(6)) {
        case 0:
            OLED_ShowString(0, 32, "Send Main", 15, 0);
            break;
        case 1:
            OLED_ShowString(0, 32, "Send Slave", 15, 0);
            break;
        case 2:
            OLED_ShowString(0, 32, "Received", 15, 0);
            break;
        default:
            break;
    }
}

void Main_Dispaly(void)
{
    OLED_FillArea(0, 0, 160, 64, 0);
    switch (getEncoderValue(6)) {
        case 0:
            OLED_ShowString(0, 0 , "CH0:     CH1:     ", 15, 0);
            OLED_ShowString(0, 16, "CH2:     CH3:     ", 15, 0);
            OLED_ShowString(0, 32, "CH4:     CH5:     ", 15, 0);
            OLED_ShowString(0, 48, "CH6:     CH7:     ", 15, 0);
            break;
        case 1:
            OLED_ShowString(0, 0 , "CH0:     CH1:     ", 15, 0);
            OLED_ShowString(0, 16, "CH2:     CH3:     ", 15, 0);
            OLED_ShowString(0, 32, "CH4:     CH5:     ", 15, 0);
            OLED_ShowString(0, 48, "CH6:     CH7:     ", 15, 0);
            break;
        case 2:
            OLED_ShowString(0, 0 , "CH0:     CH1:     ", 15, 0);
            OLED_ShowString(0, 16, "CH2:     CH3:     ", 15, 0);
            OLED_ShowString(0, 32, "CH4:     CH5:     ", 15, 0);
            OLED_ShowString(0, 48, "CH6:     CH7:     ", 15, 0);
            break;
        default:
            break;
    }
}