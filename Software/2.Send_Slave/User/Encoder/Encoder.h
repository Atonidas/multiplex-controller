#ifndef ENCODER_H
#define ENCODER_H

#include "main.h"

#define MAX_ENCODERS  10                                              // 编码器子项最大数量
#define EC_READ_PIN() HAL_GPIO_ReadPin(EC11_B_GPIO_Port, EC11_B_Pin)  // 方向判断引脚
#define EC_READ_KEY() !HAL_GPIO_ReadPin(EC11_C_GPIO_Port, EC11_C_Pin) // 按下判断引脚

typedef struct {
    int value; // 编码器的数值
    int limit; // 编码器的上限
} EncoderData;

typedef struct {
    unsigned short history_value;
    unsigned short step;
    char name[20];
    void (*service_callback)();
} EncoderService;

extern EncoderData encoders[MAX_ENCODERS];            // 编码器数据结构体数组
extern EncoderService encoder_services[MAX_ENCODERS]; // 编码器服务函数结构体数组
extern uint8_t numEncoders;                           // 当前已注册的编码器数量

void registerEncoder(unsigned short initialValue, unsigned short limit, unsigned short step, void (*service_callback)(), char *name);
unsigned short getEncoderValue(unsigned char index);
void Encoder_IT_Task(void);
void encoderServiceTask(void);

#endif /* ENCODER_H */