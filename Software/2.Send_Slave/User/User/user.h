/*
 * @Author       : error: git config user.name & please set dead value or install git
 * @LastEditors  : error: git config user.name & please set dead value or install git
 * @LastEditTime : 2023-10-05 15:11:38
 * @FilePath     : \multiplex_controller\User\User\user.h
 * @Description  : 
 */
#ifndef __USER_H__
#define __USER_H__

#include "rtthread.h"
#include "Task\task.h"

#define CHANNEL_START 0
#define CHANNEL_END 0

#define LEFT          0
#define RIGHT         1
#define UI_PIX        4

void UpdateRecordState(void);
void UpdateRecorder(uint16_t GPIO_Pin, uint8_t mode);
void Encoder_MainMenu(void);
void Main_Dispaly(void);

#endif