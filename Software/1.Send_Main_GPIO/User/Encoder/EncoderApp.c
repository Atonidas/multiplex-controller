/*
 * @Author       : Atonidas
 * @LastEditors  : Atonidas
 * @LastEditTime : 2023-10-07 11:46:21
 * @FilePath     : \1.Send_Main\User\Encoder\EncoderApp.c
 * @Description  : 编码器用户函数，请在此处具体完成编码器子项的注册函数及回调函数
 */

#include "Encoder\Encoder.h"
#include "Task\task.h"
#include "User\user.h"
#include "OLED\oled.h"

// 以下用于具体功能

// void setChannelNums(void);
// void setIntervalTime(void);
// void setGroupInterval(void);
// void setRuntime(void);
// void clearData(void);
// void setMode(void);

void Encoder_Init(void)
{
    // registerEncoder(1, 6, 1, encoder_Menu, "Main");                  // Menu: 1~6
    // registerEncoder(0, 8, 1, setChannelNums, "Channel Nums");        // Nums: 1~6
    // registerEncoder(30, 600, 1, setIntervalTime, "Interval Time");   // Interval: Init:3.0s Max:60.0s
    // registerEncoder(10, 100, 1, setGroupInterval, "Group Interval"); // Group: Init:1.0s Max:10.0s
    // registerEncoder(20, 50, 1, setRuntime, "Run Time");              // Run: Init:2.0s Max:5.0s
    // registerEncoder(0, 1, 1, clearData, "Clear Data");               // 0 or 1
    // registerEncoder(0, 2, 1, setMode, "Sys Mode");                   // 0: Send_main 1:Send 2:receive
}

void encoder_thread_entry(void *parameter)
{
    Encoder_Init();
    rt_kprintf("encoder Init is ok\r\n");
    for (;;) {
        // encoderServiceTask();
    }
}
