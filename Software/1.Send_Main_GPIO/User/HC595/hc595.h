/*
 * @Author       : error: git config user.name & please set dead value or install git
 * @LastEditors  : Atonidas
 * @LastEditTime : 2023-10-09 10:12:58
 * @FilePath     : \1.Send_Main_GPIO\User\HC595\hc595.h
 * @Description  : 
 */
#ifndef __HC595_H
#define __HC595_H

#include "main.h"

void HC595_Init(void);
void HC595_WriteByte(uint8_t data);
void HC595_Write(uint8_t channel, uint8_t state);
void HC595_Toggle(uint8_t channel);

#endif

