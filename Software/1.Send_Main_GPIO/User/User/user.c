/*
 * @Author       : Atonidas
 * @LastEditors  : Atonidas
 * @LastEditTime : 2023-11-03 15:23:17
 * @FilePath     : \1.Send_Main_GPIO\User\User\user.c
 * @Description  : 项目中的用户函数，主要为不通用的交互菜单等代码
 */

#include "OLED\oled.h"
#include "User\user.h"
#include "tim.h"
#include "HC595\hc595.h"
#include "Flash\MCU_Flash.h"

#define NUM_CHANNELS 8
#define HOLD_TIME    30 // 按下时间
#define CYCLE_TIME   32  // 按下时间

unsigned int Send_Count[8] = {0};
unsigned int Flash_Data[9] = {0};

void Main_Dispaly(void);
void Update_Display(void);

/**
 * @description: 用户主服务函数，用于UI初始化及主要参数显示界面（//TODO:）
 * @param parameter 线程入口参数，默认无
 * @return
 */
void userApp_thread_entry(void *parameter)
{
    unsigned Reset_Count = 0;

    HC595_Init();
    User_Data_Read();
    rt_thread_mdelay(500);
    OLED_Init();
    OLED_Clear_Screen(0x00);
    Main_Dispaly();

    for (;;) {
        if (HAL_GPIO_ReadPin(EC11_C_GPIO_Port, EC11_C_Pin) == 0) {
            rt_kprintf("h");
            Reset_Count++;
        } else {
            Reset_Count = 0;
        }

        if (Reset_Count > 20) {
            for (uint8_t i = 0; i < 8; i++) {
                Send_Count[i] = 0;
            }
            User_Data_Save();
            OLED_Clear_Screen(0);
            OLED_ShowString(0, 24, "Data Clear!!", 15, 0);
            rt_thread_mdelay(1000);
            OLED_Clear_Screen(0);
            Main_Dispaly();
        }
        Update_Display();
    }
}

static uint16_t Record_Timer = 0, point = 0;
void UpdateRecordState(void)
{
    uint8_t data = 0;
    Record_Timer++;
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    // rt_kprintf("%d\r\n", Record_Timer);
    if (Record_Timer < HOLD_TIME) {
        data = 0xFF & ~(1 << point);
        // rt_kprintf("%x\r\n", data);
        HC595_WriteByte(data);
    }
    if (Record_Timer >= HOLD_TIME && Record_Timer < CYCLE_TIME) {
        data = 0xFF;
        // rt_kprintf("%x\r\n",data);
        HC595_WriteByte(data);
    }
    if (Record_Timer >= CYCLE_TIME) {
        Record_Timer = 0;
        Send_Count[point]++;
        point++;
        if (point > 7) {
            point = 0;
        }
    }
}

void Main_Dispaly(void)
{
    OLED_ShowString(0, 0, "C1:       C2:     ", 15, 0);
    OLED_ShowString(0, 16, "C3:       C4:     ", 15, 0);
    OLED_ShowString(0, 32, "C5:       C6:     ", 15, 0);
    OLED_ShowString(0, 48, "C7:       C8:     ", 15, 0);
}

void Update_Display(void)
{
    OLED_ShowNumber(32, 0, Send_Count[0], 5, 15, 0);
    OLED_ShowNumber(112, 0, Send_Count[1], 5, 15, 0);
    OLED_ShowNumber(32, 16, Send_Count[2], 5, 15, 0);
    OLED_ShowNumber(112, 16, Send_Count[3], 5, 15, 0);
    OLED_ShowNumber(32, 32, Send_Count[4], 5, 15, 0);
    OLED_ShowNumber(112, 32, Send_Count[5], 5, 15, 0);
    OLED_ShowNumber(32, 48, Send_Count[6], 5, 15, 0);
    OLED_ShowNumber(112, 48, Send_Count[7], 5, 15, 0);
}

void User_Data_Read(void)
{
    STMFLASH_Read(FLASH_USER_START_ADDR, Flash_Data, 9);
    if (Flash_Data[8] == 0) {
        for (uint8_t i = 0; i < 8; i++) {
            Send_Count[i] = Flash_Data[i];
        }
    }
}

void User_Data_Save(void)
{
    for (uint8_t i = 0; i < 8; i++) {
        Flash_Data[i] = Send_Count[i];
    }
    Flash_Data[8] = 0;
    MCU_Flash_EraseProgram(FLASH_USER_START_ADDR, FLASH_USER_END_ADDR, 9, Flash_Data);
}