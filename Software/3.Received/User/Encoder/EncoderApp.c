/*
 * @Author       : Atonidas
 * @LastEditors  : error: git config user.name & please set dead value or install git
 * @LastEditTime : 2023-10-05 15:19:51
 * @FilePath     : \multiplex_controller\User\Encoder\EncoderApp.c
 * @Description  : 编码器用户函数，请在此处具体完成编码器子项的注册函数及回调函数
 */

#include "Encoder\Encoder.h"
#include "Task\task.h"
#include "User\user.h"
#include "OLED\oled.h"

void encoder_Menu(void);

// 以下用于具体功能

void clearData(void);

void Encoder_Init(void)
{
    registerEncoder(1, 2, 1, encoder_Menu, "Main");           // Menu: 1~2
    registerEncoder(0, 1, 1, clearData, "Clear Data");        // 0 or 1
}

void encoder_thread_entry(void *parameter)
{
    Encoder_Init();
    rt_kprintf("encoder Init is ok\r\n");
    for (;;) {
        encoderServiceTask();
    }
}

void encoder_Menu(void)
{
    Encoder_MainMenu();
}
void clearData(void)
{
    OLED_ShowNumber(0, 32, (int)getEncoderValue(5), 3, 15, 0);
}