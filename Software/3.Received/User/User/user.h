/*
 * @Author       : error: git config user.name & please set dead value or install git
 * @LastEditors  : Atonidas
 * @LastEditTime : 2023-10-07 10:02:42
 * @FilePath     : \3.Received\User\User\user.h
 * @Description  : 
 */
#ifndef __USER_H__
#define __USER_H__

#include "rtthread.h"
#include "Task\task.h"

#define CHANNEL_START 0
#define CHANNEL_END 1

#define LEFT          0
#define RIGHT         1
#define UI_PIX        4

void User_Data_Read(void);
void User_Data_Save(void);

void UpdateRecordState(void);
void UpdateTimer(void);
void UpdateRecorder(uint16_t GPIO_Pin, uint8_t mode);

void Encoder_MainMenu(void);
void Main_Dispaly(void);

#endif