#include "hc595.h"

static uint8_t hc595_byte = 0xff;

void HC595_WriteByte(uint8_t data)
{
    HAL_SPI_Transmit(HC595_SPI, &data, 1, 10);
}

void HC595_Write(uint8_t channel, uint8_t state)
{
    // 修改指定通道的状态
    if (state) {
        hc595_byte |= (1 << channel); // 将指定bit位置1
    } else {
        hc595_byte &= ~(1 << channel); // 将指定bit位置0
    }
    // 更新595芯片的输出状态
    HC595_WriteByte(hc595_byte);
}

void HC595_Toggle(uint8_t channel)
{
    // 翻转指定通道的状态
    hc595_byte ^= (1 << channel); // 将指定bit位翻转
    // 更新595芯片的输出状态
    HC595_WriteByte(hc595_byte);
}