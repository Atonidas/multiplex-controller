#include "Task\task.h"
#include "Encoder\Encoder.h"
#include "OLED\oled.h"

struct rt_thread encoder_thread;
static rt_uint8_t encoder_stack[512];

struct rt_thread userApp_thread;
static rt_uint8_t userApp_stack[512];

void encoder_thread_entry(void *parameter);

void userApp_thread_entry(void *parameter);

void User_TaskInit(void)
{
    rt_thread_init(&encoder_thread, "encoderThread", encoder_thread_entry, RT_NULL,
                   encoder_stack, sizeof(encoder_stack), 31, 100);
    rt_thread_startup(&encoder_thread);

    rt_thread_init(&userApp_thread, "userAppThread", userApp_thread_entry, RT_NULL,
                   userApp_stack, sizeof(userApp_stack), 31, 100);
    rt_thread_startup(&userApp_thread);
}