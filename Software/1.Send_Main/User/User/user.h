/*
 * @Author       : error: git config user.name & please set dead value or install git
 * @LastEditors  : Atonidas
 * @LastEditTime : 2023-10-07 11:56:26
 * @FilePath     : \1.Send_Main\User\User\user.h
 * @Description  : 
 */
#ifndef __USER_H__
#define __USER_H__

#include "rtthread.h"
#include "Task\task.h"

#define CHANNEL_START 0
#define CHANNEL_END 0

#define LEFT          0
#define RIGHT         1
#define UI_PIX        4
void User_Data_Read(void);
void User_Data_Save(void);

void UpdateRecordState(void);
void Main_Dispaly(void);

#endif