/*
 * @Author       : error: git config user.name & please set dead value or install git
 * @LastEditors  : Atonidas
 * @LastEditTime : 2023-10-20 11:22:20
 * @FilePath     : \4.HC595_PWM\User\HC595\hc595.h
 * @Description  : 
 */
#ifndef __HC595_H
#define __HC595_H

#include "spi.h"

#define HC595_SPI &hspi1
#define HC595_PWM_MAX 500

void HC595_DMA_Start(void);
void HC595_setPWM(uint8_t channel, uint16_t Pwm);
void HC595_WriteByte(uint8_t data);
void HC595_Write(uint8_t channel, uint8_t state);
void HC595_Toggle(uint8_t channel);

#endif

