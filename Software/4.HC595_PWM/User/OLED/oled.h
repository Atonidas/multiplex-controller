#ifndef __OLED_H
#define __OLED_H 

#include "main.h"

#define SCREEN_W 160
#define SCREEN_H 64

//-----------------OLED GPIO----------------
#define OLED_DC_Clr()  HAL_GPIO_WritePin(OLED_DC_GPIO_Port, OLED_DC_Pin, 0)//DC
#define OLED_DC_Set()  HAL_GPIO_WritePin(OLED_DC_GPIO_Port, OLED_DC_Pin, 1)

#define OLED_CS_Clr()  HAL_GPIO_WritePin(OLED_CS_GPIO_Port, OLED_CS_Pin, 0)//CS
#define OLED_CS_Set()  HAL_GPIO_WritePin(OLED_CS_GPIO_Port, OLED_CS_Pin, 1)

void OLED_FillArea(unsigned char x0, unsigned char y0, unsigned char w, unsigned char h, unsigned char color);
void OLED_Clear_Screen(unsigned char color);
void OLED_ShowString(unsigned char x0, unsigned char y0, char *s, unsigned char color, unsigned char B_color);
void OLED_ShowNumber(unsigned char x0, unsigned char y0, int num, unsigned char length, unsigned char color, unsigned char bcolor);
void General_Pic_Display(unsigned char x0, unsigned char y0, unsigned char w, unsigned char h, char *pic, unsigned char color, unsigned char Bcolor);
void OLED_Init(void);

#endif

