# 多路控制器

#### 介绍
该项目为个人项目，供同学、同事交流使用

#### 软件架构
1. 硬件开发使用     AD
2. 软件开发使用     GCC工具链，借助VSCODE的EIDE插件开发
3. 烧录说明         使用cubePrg命令行烧录，请安装相应软件并添加环境变量

#### 安装教程

1.  硬件开发软件： 略
2.  软件开发软件： 
    - [VS code中的EIDE工程介绍](http://atonidas.gitee.io/stm32-hal-tutorial/%E8%BD%AF%E4%BB%B6%E7%8E%AF%E5%A2%83/VScode%E7%9A%84%E4%BD%BF%E7%94%A8.html) - - [所使用的插件介绍](http://atonidas.gitee.io/stm32-hal-tutorial/%E8%BD%AF%E4%BB%B6%E7%8E%AF%E5%A2%83/VSCode%E6%8F%92%E4%BB%B6%E5%88%86%E4%BA%AB.html)
3.  官方开发软件： 
    - [STM32cubeMX](https://www.st.com/zh/development-tools/stm32cubemx.html)
    - [STM32cubeProgrammer](https://www.st.com/zh/development-tools/stm32cubeprog.html)
    - [VS code](https://code.visualstudio.com/Download)

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 更新日志
